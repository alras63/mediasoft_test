import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import {viteStaticCopy} from 'vite-plugin-static-copy';
import path from 'path';

export default defineConfig({
    plugins: [
        viteStaticCopy({
            targets: [
                {src: path.join(__dirname, '/resources/images'), dest: path.join(__dirname, '/public')},
            ],
        }),
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js'],
            refresh: true,
        }),
    ],
});
