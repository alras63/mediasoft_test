<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель списка разрешений
 *
 * Class PermissionList
 * @package App\Models
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class PermissionList extends Model
{
    use HasFactory;

    protected $table = 'permissions_list';

    const ATTR_ID = 'id';
    const ATTR_NAME = 'name';

    protected $fillable = [
        'id',
        'name',
        'slug'
    ];
}
