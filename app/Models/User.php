<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Модель пользователя
 *
 * Class User
 * @package App\Models
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ATTR_ID = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permissions',
        'status',
        'is_admin',
        'count_checklists',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function checklists() {
        return $this->hasMany(Checklist::class, 'user_id', 'id');
    }
}
