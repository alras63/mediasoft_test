<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель чек-листа
 *
 * Class Checklist
 * @package App\Models
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class Checklist extends Model
{
    use HasFactory;

    protected $table = 'checklist';

    const ATTR_ID = 'id';
    const ATTR_USER_ID = 'user_id';

    protected $fillable = [
        'id',
        'title',
        'description',
        'user_id'
    ];

    public function items()
    {
        return $this->hasMany(ChecklistItem::class, ChecklistItem::ATTR_CHECKLIST_ID, self::ATTR_ID)->whereNull(ChecklistItem::ATTR_CHECKLIST_ITEM_ID)->with(ChecklistItem::REL_ITEMS);
    }

    public const REL_ITEMS = 'items';
}
