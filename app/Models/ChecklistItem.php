<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель элемента чек-листа
 *
 * Class ChecklistItem
 * @package App\Models
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistItem extends Model
{
    use HasFactory;

    protected $table = 'checklist_item';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    const ATTR_ID = 'id';
    const ATTR_TITLE = 'title';
    const ATTR_DESCRIPTION = 'description';
    const ATTR_DEADLINE = 'deadline';
    const ATTR_CHECKLIST_ID = 'checklist_id';
    const ATTR_CHECKLIST_ITEM_ID = 'checklist_item_id';
    const ATTR_STATUS = 'status';

    public function items()
    {
        return $this->hasMany(ChecklistItem::class, ChecklistItem::ATTR_CHECKLIST_ITEM_ID, self::ATTR_ID);
    }

    public const REL_ITEMS = 'items';

    public function checklist()
    {
        return $this->hasOne(Checklist::class, ChecklistItem::ATTR_ID, self::ATTR_CHECKLIST_ID);
    }

    public const REL_CHECKLIST = 'checklist';

}
