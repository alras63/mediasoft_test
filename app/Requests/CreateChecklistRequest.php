<?php

namespace App\Requests;

use App\Models\Checklist;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Создание чеклиста из массива данных
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class CreateChecklistRequest extends FormRequest
{

    const PARAM_CHECKLIST_TITLE       = 'title';
    const PARAM_CHECKLIST_DESCRIPTION = 'description';
    const PARAM_CHECKLIST_USER_ID     = 'userId';
    const PARAM_ITEMS                 = 'items';
    const PARAM_ITEM_TITLE            = 'title';
    const PARAM_ITEM_CHECKLIST_ID     = 'checklistId';

    /**
     * Валидация
     *
     * @return array
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function rules(): array
    {
        return [
            self::PARAM_CHECKLIST_TITLE                               => ['required', 'string'],
            self::PARAM_CHECKLIST_DESCRIPTION                         => ['string'],
            self::PARAM_CHECKLIST_USER_ID                             => [Rule::exists(User::class, User::ATTR_ID)],
            self::PARAM_ITEMS . '.*.' . self::PARAM_ITEM_TITLE        => ['required', 'string'],
        ];
    }
}
