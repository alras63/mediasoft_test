<?php

namespace App\Requests;

use App\Models\Checklist;
use App\Models\ChecklistItem;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Создание элемента чеклиста из массива данных
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class CreateChecklistItemRequest extends FormRequest
{

    const PARAM_CHECKLIST_TITLE       = 'title';
    const PARAM_CHECKLIST_DESCRIPTION = 'description';
    const PARAM_CHECKLIST_ITEM_ID     = 'checklistItemId';
    const PARAM_ITEMS                 = 'items';
    const PARAM_ITEM_TITLE            = 'title';

    /**
     * Валидация
     *
     * @return array
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function rules(): array
    {
        return [
            self::PARAM_CHECKLIST_TITLE                        => ['required', 'string'],
            self::PARAM_CHECKLIST_DESCRIPTION                  => ['string'],
            self::PARAM_CHECKLIST_ITEM_ID                      => [Rule::exists(ChecklistItem::class, ChecklistItem::ATTR_ID)],
            self::PARAM_ITEMS . '.*.' . self::PARAM_ITEM_TITLE => ['required', 'string']
        ];
    }
}
