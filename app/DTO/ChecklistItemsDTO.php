<?php

namespace App\DTO;

use App\Models\ChecklistItem;
use Illuminate\Support\Collection;

/**
 * ДТО элементов чеклиста
 *
 * Class ChecklistItemsDTO
 * @package App\DTO
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistItemsDTO
{
    const ATTR_ID = 'id';
    const ATTR_TITLE = 'title';
    const ATTR_DESCRIPTION = 'description';
    const ATTR_DEADLINE = 'deadline';
    const ATTR_CHECKLIST_ID = 'checklistId';
    const ATTR_CHECKLIST_ITEM_ID = 'checklist_item_id';
    const ATTR_ITEMS = 'items';

    /** Идентификатор */
    public int $id;
    /** Наименование */
    public string $title;
    /** Описание */
    public ?string $description;
    /** Дата */
    public ?string $deadline;
    /** ИД чеклиста */
    public int $checklistId;
    /** Статус чеклиста */
    public ?string $status;
    /** ИД элемента чеклиста */
    public ?int $checklistItemId;
    public ChecklistItemsDTO|array|null $items;

    /**
     * Конструктор класса
     *
     * @param array $data Массив данных
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function __construct( array $data = [] )
    {
        if (count($data) > 0) {
            foreach ($data as $property => $value) {
                if (property_exists($this, $property)) {
                    $this->$property = $value;
                }
            }
        }
    }

    /**
     * Конвертация данных в DTO
     *
     * @param ChecklistItem|Collection $data Модель
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public static function convertToDto( ChecklistItem|Collection $data ): static|array
    {

        if($data instanceof Collection) {

            $result = [];

            $data->each(function ($dataItem) use (&$result) {
                $dto                  = new static();
                $dto->id              = $dataItem->id;
                $dto->title           = $dataItem->title;
                $dto->description     = $dataItem->description;
                $dto->deadline        = $dataItem->deadline;
                $dto->checklistId     = $dataItem->checklist_id;
                $dto->checklistItemId = $dataItem->checklist_item_id;
                $dto->items           = $dataItem->items ? self::convertToDto($dataItem->items) :NULL;
                $dto->status          = ($dataItem->status ?  ($dataItem->status == 0 ? 'Не выполнена' : 'Выполнена') : 'Не выполнена');

                $result[] = $dto;
            });

            return $result;
        } else {
            $dto                  = new static();
            $dto->id              = $data->id;
            $dto->title           = $data->title;
            $dto->description     = $data->description;
            $dto->deadline        = $data->deadline;
            $dto->checklistId     = $data->checklist_id;
            $dto->checklistItemId = $data->checklist_item_id;
            $dto->items           = $data->items ? self::convertToDto($data->items) :NULL;
            $dto->status          = ($data->status ?  ($data->status == 0 ? 'Не выполнена' : 'Выполнена') : 'Не выполнена');
        }


        return $dto;
    }
}

