<?php

namespace App\DTO;

use App\Models\Checklist;

/**
 * ДТО чеклиста
 *
 * Class ChecklistDTO
 * @package App\DTO
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistDTO
{
    const ATTR_ID = 'id';
    const ATTR_TITLE = 'title';
    const ATTR_DESCRIPTION = 'description';
    const ATTR_USER_ID = 'userId';
    const ATTR_ITEMS = 'items';
    /** Идентификатор */
    public int $id;
    /** Наименование */
    public string $title;
    /** Описание */
    public ?string $description;
    /** ИД юзера */
    public string $userId;
    /** ИД юзера */
    public ChecklistItemsDTO|array|null $items;

    /**
     * Конструктор класса
     *
     * @param array $data Массив данных
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function __construct( array $data = [] )
    {
        if (count($data) > 0) {
            foreach ($data as $property => $value) {
                if (property_exists($this, $property)) {
                    $this->$property = $value;
                }
            }
        }
    }

    /**
     * Конвертация данных в DTO
     *
     * @param Checklist $data Модель
     *
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public static function convertToDto( Checklist $data ): static
    {
        $dto              = new static();
        $dto->id          = $data->id;
        $dto->title       = $data->title;
        $dto->description = $data->description;
        $dto->items       = $data->items ? ChecklistItemsDTO::convertToDTO($data->items) : null;
        $dto->userId      = $data->user_id;

        return $dto;
    }
}

