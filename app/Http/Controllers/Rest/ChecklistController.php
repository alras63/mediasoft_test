<?php

namespace App\Http\Controllers\Rest;

use App\DTO\ChecklistDTO;
use App\DTO\ChecklistItemsDTO;
use App\Http\Controllers\Controller;
use App\Models\Checklist;
use App\Models\User;
use App\Models\ChecklistItem;
use App\Requests\CreateChecklistRequest;
use App\Services\ChecklistService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * REST-контроллер чек-листов
 *
 * Class ChecklistController
 * @package App\Http\Controllers\Rest
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistController extends Controller
{
    public function list()
    {
        $checklists = Checklist::with(Checklist::REL_ITEMS)
            ->where(Checklist::ATTR_USER_ID, Auth::id())->whereHas(Checklist::REL_ITEMS, function ( $q ) {
                $q->whereNull(ChecklistItem::ATTR_CHECKLIST_ITEM_ID);
            })->get();

        $result = [];

        foreach ($checklists as $checklist) {
            array_push($result, ChecklistDTO::convertToDto($checklist));
        }

        return Response::json(['data' => $result]);
    }

    public function create( CreateChecklistRequest $request )
    {
        $userChecklists = Checklist::where(Checklist::ATTR_USER_ID, Auth::id())->get();
        $user = User::where(User::ATTR_ID, Auth::id())->first();



        if(($user && ($userChecklists->count() + 1 <= $user->count_checklists)) || ($user && $user->count_checklists == NULL)) {
            $checklist = ( new ChecklistService )->create($request);
        } else {
            return Response::json(['data' => ' Вы превысили разрешенное количество чек-листов'], ResponseAlias::HTTP_BAD_REQUEST);
        }


        if ($checklist) {
            return Response::json(['data' => $checklist]);
        }
        else {
            return Response::json(['data' => 'Не удалось создать чек-лист с заданными параметрами'], ResponseAlias::HTTP_BAD_REQUEST);
        }
    }

    public function delete( int $id )
    {
        $checklist = Checklist::where(Checklist::ATTR_ID, $id)->first();

        if ($checklist->user_id === Auth::id()) {
            $checklist->delete();

            return Response::json(['data' => 'Успешно удалено']);
        }
        else {
            return Response::json(['data' => 'Нет доступа'], ResponseAlias::HTTP_UNAUTHORIZED);
        }

    }

    public function items( $checklistId )
    {
        $checklistItems = ChecklistItem::with([ChecklistItem::REL_ITEMS])->whereHas(ChecklistItem::REL_CHECKLIST, function ( $query ) use ( $checklistId ) {
            $query->where(Checklist::ATTR_ID, '=', $checklistId);
            $query->where(Checklist::ATTR_USER_ID, '=', Auth::id());
            return $query;
        })->whereNull(ChecklistItem::ATTR_CHECKLIST_ITEM_ID)->get();

        if ($checklistItems->count() === 0) {
            return Response::json(['data' => 'Элементы не найдены'], ResponseAlias::HTTP_NOT_FOUND);
        }

        $result = [];

        foreach ($checklistItems as $checklistItem) {
            array_push($result, ChecklistItemsDTO::convertToDto($checklistItem));
        }

        return Response::json(['data' => $result]);

    }
}
