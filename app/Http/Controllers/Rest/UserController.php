<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * REST-контроллер для юзеров
 *
 * Class UserController
 * @package App\Http\Controllers\Rest
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class UserController extends Controller
{

    public function auth( Request $request )
    {
        $patternStr = '(?:[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';

        $validate = Validator::make($request->all(), [
            'email'    => ['required', "regex:/$patternStr/"],
            'password' => ['required', 'string', 'min:8']
        ],
            [
                'email.regex' => 'Это не email',
                'email.required' => 'Email обязателен',

                'password.min' => 'Пароль минимум 8 символов',
                'password.required' => 'Пароль обязателен',
            ]);

        if ($validate->fails()) {
            return Response::json($validate->errors()->toArray());
        }

        $validate = $validate->validated();

        if (!Auth::validate($validate)) {
            return Response::json(['data' => 'Некорректный логин или пароль'], ResponseAlias::HTTP_BAD_REQUEST);
        }

        $user = Auth::getProvider()->retrieveByCredentials($validate);

        $token = Str::random(80);

        $userModel = User::where('id', $user->getAuthIdentifier())->first();

        $userModel->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return Response::json(['data' => 'Успешная авторизация', 'token' => $token]);
    }

    public function register( Request $request )
    {
        $patternStr = '(?:[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';

        $validate = Validator::make($request->all(), [
            'name'     => ['required', 'regex:/^[a-zA-Z0-9 ]+$/'],
            'email'    => ['required', "regex:/$patternStr/", 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        ],
            [
                'name.regex' => 'Имя пользователя должно быть на латинице',
                'name.required' => 'Имя пользователя обязательно',

                'email.regex' => 'Это не email',
                'email.required' => 'Email обязателен',

                'password.min' => 'Пароль минимум 8 символов',
                'password.required' => 'Пароль обязателен',
            ]);


        if ($validate->fails()) {
            return Response::json($validate->errors()->toArray());
        }

        $validate = $validate->validated();

        $user = new User();
        $user->fill(collect($validate)->toArray())->fill(['status' => 1, 'permissions' => json_encode([]), 'is_admin' => 0]);
        $user->password = Hash::make($validate['password']);
        try {
           $user->save();
        }
        catch(\Exception $e){
            return Response::json(['data' => $e->getMessage()], ResponseAlias::HTTP_BAD_REQUEST);
        }

        if (!Auth::validate($validate)) {
            return Response::json(['data' => 'Некорректный логин или пароль'], ResponseAlias::HTTP_BAD_REQUEST);
        }

        $user = Auth::getProvider()->retrieveByCredentials($validate);

        $token = Str::random(80);

        $userModel = User::where('id', $user->getAuthIdentifier())->first();

        $userModel->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return Response::json(['data' => 'Успешная регистрация', 'token' => $token]);
    }
}
