<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use App\Models\Checklist;
use App\Models\ChecklistItem;
use App\Requests\CreateChecklistItemRequest;
use App\Services\ChecklistService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * REST-контроллер элементов чек-листов
 *
 * Class ChecklistItemController
 * @package App\Http\Controllers\Rest
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistItemController extends Controller
{
    public function create( $checklistId, CreateChecklistItemRequest $request )
    {
        $checklist = Checklist::where(Checklist::ATTR_ID, $checklistId)
            ->where(Checklist::ATTR_USER_ID, Auth::id())->first();

        if ($checklist === null) {
            return Response::json(['data' => 'Ошибка доступа'], ResponseAlias::HTTP_BAD_REQUEST);
        }

        $res = ChecklistService::insertItems($request->toArray(), $checklistId);

        if ($res) {
            return Response::json(['data' => 'Успешно создано']);
        }

        return Response::json(['data' => 'Ошибка'], ResponseAlias::HTTP_BAD_REQUEST);

    }

    public function delete( $checklistId, $itemId )
    {
        $checklist = Checklist::where(Checklist::ATTR_ID, $checklistId)->first();

        if ($checklist->user_id === Auth::id()) {
            $checklistItem = ChecklistItem::where(ChecklistItem::ATTR_ID, $itemId)->first();
            if ($checklistItem !== null) {
                if ($checklistItem->checklist->id === $checklist->id) {
                    $checklistItem->delete();
                    return Response::json(['data' => 'Успешно удалено']);
                }
            }
        }

        return Response::json(['data' => 'Нет элемента'], ResponseAlias::HTTP_BAD_REQUEST);

    }

    public function editStatus( $checklistId, $itemId, Request $request )
    {
        $validate = Validator::make($request->all(), [
            'status'      => ['required', 'integer', 'between:0,1']
        ]);

        if ($validate->fails()) {
            return Response::json($validate->errors()->toArray());
        }

        $validate = $validate->validated();

        $checklist = Checklist::where(Checklist::ATTR_ID, $checklistId)->first();

        if ($checklist->user_id === Auth::id()) {
            $checklistItem = ChecklistItem::where(ChecklistItem::ATTR_ID,  $itemId)->first();
            if ($checklistItem !== null) {
                if ($checklistItem->checklist->id === $checklist->id) {
                    $checklistItem->status = $validate['status'];

                    if($checklistItem->save()) {
                        return Response::json(['data' => 'Статус успешно сохранен']);
                    }
                }
            }
        }

        return Response::json(['data' => 'Нет элемента'], ResponseAlias::HTTP_BAD_REQUEST);
    }
}
