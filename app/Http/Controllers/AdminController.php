<?php

namespace App\Http\Controllers;

use App\Models\PermissionList;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

/**
 * Контроллер, отвевечающий за админку
 *
 * Class AdminController
 * @package App\Http\Controllers
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class AdminController extends Controller
{
    public function index()
    {
        return redirect('/admin/checklist/');
    }

    public function checklist()
    {
        $users = User::all();
        $userAuth       = User::where('id', Auth::id())->first();
        return view('admin/checklist', ['users' => $users, 'userAuth' => $userAuth]);
    }

    public function users_view()
    {
        $users          = User::all();
        $userAuth       = User::where('id', Auth::id())->first();
        $permissionList = PermissionList::all();
        return view('admin/users', ['users' => $users, 'permissionList' => $permissionList, 'userAuth' => $userAuth]);
    }

    public function users_status( Request $request )
    {
        $validate = Validator::make($request->all(), [
            'id'     => ['required',
                         'integer'],
            'status' => ['required',
                         'integer', 'between:0,1'],
        ]);
        if ($validate->fails()) {
            return Response::json($validate->errors()->toArray());
        }

        $validate = $validate->validated();

        $user = User::where('id', $validate[User::ATTR_ID])->first();
        $user->fill($validate);

        if (!$user->save()) {
            return Response::json(['message' => 'Произошла ошибка при сохранении']);
        }
        return Response::json(['message' => 'Успешно сохранено']);

    }

    public function users_edit( Request $request )
    {
        $validate = Validator::make($request->all(), [
            'id'               => ['required',
                                   'integer'],
            'name'             => ['required',
                                   'string'],
            'email'            => ['required',
                                   'string'],
            'count_checklists' => ['integer', 'nullable', 'min:0'],
            'status'           => ['required',
                                   'integer', 'between:0,1'],
            'is_admin'         => ['string', 'nullable'],

            'permissions' => ['array', 'nullable'],
        ],
        [
            'count_checklists.min' => 'Количество возможных чеклистов должно быть не меньше 0'
        ]);
        if ($validate->fails()) {
            return Response::redirectTo('/admin/users/')->withErrors($validate->errors()->toArray());
        }

        $validate = $validate->validated();

        $user = User::where('id', $validate[User::ATTR_ID])->first();
        $user->fill($validate);

        $user->permissions = ( isset($validate['permissions']) ? json_encode($validate['permissions']) : NULL);
        $user->is_admin    = ( isset($validate['is_admin']) && $validate['is_admin'] == 'on' ? 1 : 0 );

        if (!$user->save()) {
            return Response::redirectTo('/admin/users/')->withErrors(['message' => 'Произошла ошибка при сохранении']);
        }
        return Response::redirectTo('/admin/users/');
    }
}
