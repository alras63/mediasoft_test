<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Middleware, отвечающий за админчкие запросы
 *
 * Class AdminMiddleware
 * @package App\Http\Middleware
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class AdminMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->is_admin == 1) {
            return $next($request);
        }
        Auth::logout();
        return Redirect::to('/user/auth/');
    }
}
