<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Response;

/**
 * Middleware, блокирующий запросы от заблокированных пользователей
 *
 * Class ActiveMiddleware
 * @package App\Http\Middleware
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ActiveMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->status) {
            return $next($request);
        } else {
            return Response::json(['message' => 'Вы заблокированы'], \Illuminate\Http\Response::HTTP_FORBIDDEN);
        }
    }
}
