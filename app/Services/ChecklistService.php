<?php
namespace App\Services;

use App\DTO\ChecklistDTO;
use App\Models\Checklist;
use App\Models\ChecklistItem;
use App\Requests\CreateChecklistRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * Сервис для работы с чек-листами
 *
 * Class ChecklistService
 * @package App\Services
 *
 * @author Рассохин Алексей <telegram: @alras63>
 */
class ChecklistService
{

    public function create( CreateChecklistRequest $checklistData )
    {
        DB::beginTransaction();
        $checklist              = new Checklist();
        $checklist->title       = $checklistData[$checklistData::PARAM_CHECKLIST_TITLE];
        $checklist->description = $checklistData[$checklistData::PARAM_CHECKLIST_DESCRIPTION];
        $checklist->user_id     = $checklistData[$checklistData::PARAM_CHECKLIST_USER_ID] ?? Auth::id();

        if ($checklist->save()) {

            if( isset($checklistData[$checklistData::PARAM_ITEMS]) && count($checklistData[$checklistData::PARAM_ITEMS]) > 0  ) {
                foreach ($checklistData[$checklistData::PARAM_ITEMS] as $dataItem) {
                    static::insertItems($dataItem, $checklist->id);
                }
            }
            DB::commit();

            $checklist = $checklist->fresh(Checklist::REL_ITEMS);

            return ChecklistDTO::convertToDto($checklist);
        }
        else {
            DB::rollBack();
        }

        return false;
    }

    public static function insertItems( array $dataItem, int $checklistId, int $checklistItemId = NULL) {
        try {
            $checklistItem = new ChecklistItem();
            $checklistItem->title = $dataItem[ChecklistItem::ATTR_TITLE];
            $checklistItem->description = $dataItem[ChecklistItem::ATTR_DESCRIPTION] ?? NULL;
            $checklistItem->deadline = $dataItem[ChecklistItem::ATTR_DEADLINE] ?? NULL;
            $checklistItem->checklist_id = $checklistId;
            $checklistItem->checklist_item_id = $checklistItemId ?? NULL;

            $checklistItem->save();

            if( isset($dataItem['items']) && count($dataItem['items']) > 0  ) {
                foreach ($dataItem['items'] as $dataItemInsert) {
                    static::insertItems($dataItemInsert, $checklistId, $checklistItem->id);
                }
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}

?>
