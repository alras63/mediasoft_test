<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::guest()) {
        return redirect('/user/auth/');
    } else {
        return redirect('/admin/index');
    }

});

Route::group(['middleware' => ['guest']], function () {
    Route::match(array('GET',
                       'POST'), '/user/auth/', function (Request $request) {
        return (new \App\Http\Controllers\UserController())->auth($request);
    })->name('auth');

    Route::match(array('GET',
                       'POST'), '/user/register/', function (Request $request) {
        return (new \App\Http\Controllers\UserController())->register($request);
    })->name('register');
});

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::match(array('GET',), '/user/logout/', function () {
        return (new \App\Http\Controllers\UserController())->logout();
    })->name('logout');

    Route::get('/admin/index', function () {
        return (new \App\Http\Controllers\AdminController())->index();
    })->middleware('admin');

    Route::get('/admin/checklist', function () {
        return (new \App\Http\Controllers\AdminController())->checklist();
    })->middleware('admin')->name('checklist');

    Route::match(array('GET'), '/admin/users', [\App\Http\Controllers\AdminController::class,
                                                'users_view'])->middleware('admin')->name('usersView');
    Route::match(array('POST'), '/admin/users/edit', [\App\Http\Controllers\AdminController::class,
                                                      'users_edit'])->middleware('admin')->name('editUser');
    Route::match(array('POST'), '/admin/users-status', [\App\Http\Controllers\AdminController::class,
                                                        'users_status'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class])->middleware('admin')->name('editStatusUser');

});
