<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('guest')->post('/auth', [\App\Http\Controllers\Rest\UserController::class, 'auth']);
Route::middleware('guest')->post('/register', [\App\Http\Controllers\Rest\UserController::class, 'register']);


Route::group(['middleware' => ['auth:api', 'active', 'cors']], function () {
    Route::get('/checklist', [\App\Http\Controllers\Rest\ChecklistController::class, 'list']);
    Route::get('/checklist/{checklistId}/items', [\App\Http\Controllers\Rest\ChecklistController::class, 'items']);
    Route::delete('/checklist/{checklistId}/items/{itemId}/delete', [\App\Http\Controllers\Rest\ChecklistItemController::class, 'delete']);
    Route::post('/checklist/{checklistId}/items/{itemId}/editStatus', [\App\Http\Controllers\Rest\ChecklistItemController::class, 'editStatus']);
    Route::post('/checklist/{checklistId}/items/create', [\App\Http\Controllers\Rest\ChecklistItemController::class, 'create']);
    Route::post('/checklist/create', [\App\Http\Controllers\Rest\ChecklistController::class, 'create']);
    Route::delete('/checklist/delete/{id}', [\App\Http\Controllers\Rest\ChecklistController::class, 'delete']);

});
