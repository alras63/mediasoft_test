@extends('admin/index')

@section('admin-content')
    @if( isset($userAuth->permissions) && $userAuth->permissions !== NULL && in_array('view-checklist', json_decode($userAuth->permissions)))
        <div class="grid grid-cols-3 gap-4 grid-no-tablet">
            @foreach($users as $user)
                <div class="max-w-lg my-3 bg-white p-8 rounded-xl shadow shadow-slate-300">
                    <div class="flex flex-row justify-between items-center">
                        <div>
                            <h1 class="text-3xl font-medium">{{$user->name}}</h1>
                        </div>
                    </div>
                    <div id="tasks" class="my-5">
                        @foreach($user->checklists as $checklist)
                            <div id="task" class="overflow-x-scroll flex justify-between items-center border-b border-slate-200 py-3 px-2 border-l-4  border-l-transparent">
                                <div class="inline-flex items-start space-x-2 flex-col">
                                    <h3 class="text-xl p-2">#{{$checklist->id}} {{$checklist->title}}</h3>
                                    <p>
                                        {{$checklist->description}}
                                    </p>
                                    <p class="p-2">
                                        Задачи:
                                    </p>
                                    @include('admin/checklistItem', ['checklistItems' => $checklist->items])
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            @endforeach
        </div>
    @else
        <p class="text-lg font-bold p-4 text-red-800">Ваша админская роль не позволяет смотреть список чек-листов</p>
    @endif

@endsection
