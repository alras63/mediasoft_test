@extends('admin/index')
@section('admin-content')
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        @if( isset($userAuth->permissions) && $userAuth->permissions !== NULL && in_array('view-user', json_decode($userAuth->permissions)))

            @if(isset($errors) && $errors->count() > 0)
                <div class="px-5 py-7 rounded-md bg-red-800 text-white font-bold mb-3" >
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                </div>
            @endif

            <table class="w-full overflow-x-scroll text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        ID
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Имя пользователя
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Email
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Разреш. кол-во чеклистов
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Статус
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Роли
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Админ?
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Действие
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            {{$user->id}}
                        </th>
                        <td class="px-6 py-4">
                            {{$user->name}}
                        </td>
                        <td class="px-6 py-4">
                            {{$user->email}}
                        </td>
                        <td class="px-6 py-4">
                            {{$user->count_checklists}}
                        </td>
                        <td class="px-6 py-4">

                            {{$user->status}}
                        </td>
                        <td class="px-6 py-4">
                            @php
                                if(isset($user) && isset($permissionList)) {
                                    $permissions = json_decode($user->permissions);
                                        if(isset($permissions) && count($permissions) > 0) {
                                            foreach ($permissions as $permission) {
                                            $permName = $permissionList->where('slug', $permission)->first();

                                            echo "<p>$permName->name</p>";
                                        }
                                    }

                                }
                            @endphp
                        </td>

                        <td class="px-6 py-4">
                            {{$user->is_admin ? 'Да' : 'Нет'}}
                        </td>
                        <td class="px-6 py-4 flex flex-col flex-shrink-0 gap-2">
                            <a href="#"
                               class="font-medium text-white bg-blue-500 text-center py-2 px-3 rounded-lg hover:underline openModal"
                               style="margin-bottom: 3px"
                               data-id="{{$user->id}}"
                            >Редактировать</a>
                            <a href="#"
                               data-id="{{$user->id}}"
                               data-status="{{$user->status}}"
                               class="statusUserBtn px-3 font-medium text-white {{$user->status == 1 ? "bg-red-500" : "bg-green-500"}} text-center py-2 rounded-lg hover:underline">{{($user->status == 1 ? "Заблокировать" : "Разблокировать")}}</a>

                        </td>

                        <!-- Overlay element -->
                        <div id="overlay"
                             data-overlay-id="{{$user->id}}"
                             class="overlayBlock fixed hidden z-40 w-screen h-screen inset-0 bg-gray-900 bg-opacity-60"></div>

                        <!-- The dialog -->
                        <div id="dialog"
                             data-dialog-id="{{$user->id}}"
                             class="hidden fixed z-50 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-96 bg-white rounded-md px-8 py-6 space-y-5 drop-shadow-lg">
                            <h1 class="text-2xl font-semibold">Карточка {{$user->name}}</h1>
                            <div class="py-5 border-t border-b border-gray-300">

                                <form action="{{route('editUser')}}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{$user->id}}" name="id"/>
                                    <label class="font-semibold text-sm text-gray-600 pb-1 block">Имя пользователя</label>
                                    <input type="text" value="{{$user->name}}" name="name"
                                           class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full"/>
                                    <label class="font-semibold text-sm text-gray-600 pb-1 block">E-mail</label>
                                    <input type="text" value="{{$user->email}}" name="email"
                                           class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full"/>
                                    <select type="text" name="status"
                                            class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full">
                                        <option value="1" {{$user->status == 1 ? 'checked' : ''}}>Активный</option>
                                        <option value="0" {{$user->status == 0 ? 'checked' : ''}}>Заблокирован</option>
                                    </select>

                                    @if(isset($userAuth) && isset($userAuth->permissions) && $userAuth->permissions !== NULL && in_array('setting-user', json_decode($userAuth->permissions)))
                                        <label class="font-semibold text-sm text-gray-600 pb-1 block">Разрешенное количество чек-листов</label>
                                        <input type="text" value="{{$user->count_checklists}}" name="count_checklists"
                                               class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full"/>
                                    @endif

                                    @if($user->is_admin)
                                        <h2 class="text-lg font-bold mb-2">Что может делать этот админ?</h2>
                                    <div class="flex flex-col">
                                        @foreach($permissionList as $index=>$permissionListItem)
                                        <div class="flex items-center mb-4">
                                            <input id="permissions_{{$index}}" type="checkbox" name="permissions[]" value="{{$permissionListItem->slug}}"
                                                   {{isset($user->permissions) && $user->permissions !== null && in_array($permissionListItem->slug, json_decode($user->permissions) )  ? 'checked' : ''}}
                                                   class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600  focus:ring-2">
                                            <label for="permissions_{{$index}}"
                                                   class="ml-2 text-sm font-medium text-gray-900">
                                                {{$permissionListItem->name}}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif


                                    <div class="mt-2">
                                        <h2 class="text-lg font-bold mb-2">Этот пользователь - админ?</h2>
                                        <div class="flex items-center mb-4">
                                            <input id="permissions_admin" type="checkbox" name="is_admin"
                                                   {{$user->is_admin ? "checked" : ''}}
                                                   class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500  focus:ring-2">
                                            <label for="permissions_admin"
                                                   class="ml-2 text-sm font-medium text-gray-900">Админ</label>
                                        </div>
                                    </div>
                                    <button type="submit"
                                            class="transition duration-200 bg-blue-500 hover:bg-blue-600 focus:bg-blue-700 focus:shadow-sm focus:ring-4 focus:ring-blue-500 focus:ring-opacity-50 text-white w-full py-2.5 rounded-lg text-sm shadow-sm hover:shadow-md font-semibold text-center inline-block">
                                        <span class="inline-block mr-2">Изменить</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </tr>

                @endforeach
                </tbody>
            </table>
        @else
            <p class="text-lg font-bold p-4 text-red-800">Ваша админская роль не позволяет смотреть список пользователей (и админов)</p>
        @endif
    </div>




    <script>
        let openButton = document.querySelectorAll('.openModal');
        let overlayClick = document.querySelectorAll('.overlayBlock');

        overlayClick.forEach(ov => {
            let dataId = ov.getAttribute('data-overlay-id');
            let dialog = document.querySelector('[data-dialog-id="' + dataId + '"]');

            ov.addEventListener('click', function () {
                console.log(123213);
                dialog.classList.add('hidden');
                ov.classList.add('hidden');
            })


        });

        openButton.forEach(btn => {
            let dataId = btn.getAttribute('data-id');
            let dialog = document.querySelector('[data-dialog-id="' + dataId + '"]');
            let overlay = document.querySelector('[data-overlay-id="' + dataId + '"]');
            console.log(dialog);

            btn.addEventListener('click', function () {
                dialog.classList.remove('hidden');
                overlay.classList.remove('hidden');
            })
        });

        async function postData(url = "", data = {}) {
            const response = await fetch(url, {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                },
                redirect: "follow",
                referrerPolicy: "no-referrer",
                body: JSON.stringify(data),
            });
            return response.json();
        }


        let statusUserBtn = document.querySelectorAll('.statusUserBtn');
        statusUserBtn.forEach(btn => {

            btn.addEventListener('click', function () {
                let dataId = btn.getAttribute('data-id');
                let dataStatus = btn.getAttribute('data-status');
                let status = (dataStatus === "1" ? "0" : "1");
                console.log(dataId, status);
                postData("/admin/users-status", { id: dataId, status: status }).then((data) => {
                   location.reload();
                });
            })

        });
    </script>



@endsection
