@extends('./../layouts/layout')

@section('content')
    <div class="flex flex-row min-h-screen bg-gray-100 text-gray-800">
        <aside
            class="sidebar w-64 md:shadow transform -translate-x-full md:translate-x-0 transition-transform duration-150 ease-in bg-dark bg-black"
        >
            <div class="sidebar-header flex items-center justify-center py-4">
                <div class="inline-flex">
                    <a href="#" class="inline-flex flex-row items-center p-6">
                        <img src="{{ asset('/images/header-logo-p.png') }}"
                             alt="">
                    </a>
                </div>

            </div>

            <div class="sidebar-content px-4 py-6">
                <ul class="flex flex-col w-full">
                    <p class="text-lg font-bold text-white p-4">Привет, {{isset($userAuth) ? $userAuth->name : ''}}</p>

                    <li class="my-px">
                        <span class="flex font-medium text-sm text-gray-300 px-4 my-4 uppercase">Чек-листы</span>
                    </li>
                    <li class="my-px">
                        <a
                            href="{{route('checklist', [])}}"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                        >
              <span class="flex items-center justify-center text-lg text-gray-400">
                <svg
                    fill="none"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    class="h-6 w-6"
                >
                  <path
                      d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"
                  />
                </svg>
              </span>
                            <span class="ml-3">Просмотр</span>
                        </a>
                    </li>

                    <li class="my-px">
                        <span class="flex font-medium text-sm text-gray-300 px-4 my-4 uppercase">Профили</span>
                    </li>
                    <li class="my-px">
                        <a
                            href="{{route('usersView')}}"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                        >
              <span class="flex items-center justify-center text-lg text-gray-400">
                <svg
                    fill="none"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    class="h-6 w-6"
                >
                  <path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"/>
                </svg>
              </span>
                            <span class="ml-3">Пользователи и админы</span>
                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="{{route('logout')}}"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                        >
              <span class="flex items-center justify-center text-lg text-red-400">
                <svg
                    fill="none"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    class="h-6 w-6"
                >
                  <path
                      d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z"
                  />
                </svg>
              </span>
                            <span class="ml-3">Выход</span>
                        </a>
                    </li>
                </ul>
            </div>
        </aside>
        <main class="main flex flex-col flex-grow -ml-64 md:ml-0 transition-all duration-150 ease-in overflow-x-scroll ">
            <div class="main-content flex flex-col flex-grow p-4">
                <h1 class="font-bold text-2xl text-gray-700">Админ-панель</h1>

                @yield('admin-content')
            </div>
            <footer class="footer px-4 py-6">
                <div class="footer-content">
                    <p class="text-sm text-gray-600 text-center">© Медиасофт 2023. Все права защищены</p>
                </div>
            </footer>
        </main>
    </div>

@endsection
