<div>
    @php
        $index = $index ?? 2;
    @endphp

    <ol style="padding-left: calc(15px * {{$index}})">

    @if(isset($checklistItems) && count($checklistItems) > 0)
    @foreach($checklistItems as $checklistItem)
                <li class="py-3 border-l-4 border-black border-solid" style="padding-left: 20px">
                    <span>
                       <b>#{{$checklistItem->id}} {{$checklistItem->title}}</b> {{$checklistItem->description ? ' / ' .  $checklistItem->description : ''}} <br>
                        <span class="mt-1 inline-block p-2 text-white  {{($checklistItem->status ?  ($checklistItem->status == 0 ? 'bg-red-700' : 'bg-green-700') : 'bg-red-700')}} rounded-md"> {{($checklistItem->status ?  ($checklistItem->status == 0 ? 'Не выполнена' : 'Выполнена') : 'Не выполнена')}}</span>
                    </span>
                </li>
                @if(isset($checklistItem->items) && count($checklistItem->items) > 0)
                <li class="py-3 my-3" >
                    Дочерние задачи:
                    @include('admin/checklistItem', ['checklistItems' => $checklistItem->items, $index+1])

                </li>
                @endif
    @endforeach
    @endif
    </ol>
</div>
