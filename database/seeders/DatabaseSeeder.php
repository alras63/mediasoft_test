<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('permissions_list')->insert([
            'name' => 'Просмотр чек-листов',
            'slug' => 'view-checklist',
        ]);

        DB::table('permissions_list')->insert([
            'name' => 'Просмотр пользователей',
            'slug' => 'view-user',
        ]);

        DB::table('permissions_list')->insert([
            'name' => 'Ограничение кол-ва чеклистов у пользователя',
            'slug' => 'setting-user',
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@nomail.co',
            'password' => Hash::make('adminMediasoft123'),
            'is_admin' => 1,
            'status' => 1,
            'permissions' => json_encode(['view-checklist', 'view-user', 'setting-user'])
        ]);
    }
}
