<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('checklist_item', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('checklist_id')->comment('К какому чек-листу');
            $table->unsignedBigInteger('checklist_item_id')->nullable()->comment('К какому пункту (вложенность)');
            $table->string('title')->comment('Наименование пункта');
            $table->string('description')->nullable()->comment('Описание пунка');
            $table->dateTime('deadline')->nullable()->comment('Дедлайн');
            $table->integer('status')->nullable()->comment('Статус');

            //связи
            $table->foreign('checklist_id')->references('id')->on('checklist')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('checklist_item');
    }
};
