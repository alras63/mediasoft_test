<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Имя пользователя');
            $table->string('email')->comment('Электронная почта')->unique();
            $table->string('password')->comment('Пароль');
            $table->rememberToken()->nullable()->comment('Токен');
            $table->boolean('is_admin')->nullable()->comment('Является ли админом');
            $table->integer('status')->nullable()->comment('Статус');
            $table->json('permissions')->nullable()->comment('Права пользователя');
            $table->integer('count_checklists')->nullable()->comment('Количество разрешенных чеклистов');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
